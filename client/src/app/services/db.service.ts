import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthService } from './auth.service';

@Injectable()
export class DbService {

  domain = this.authService.domain;
  options;

  constructor(
    private http: Http,
    private authService: AuthService
  ) { }


  // Function to create headers, add token, to be used in HTTP requests
  createAuthenticationHeaders() {
    this.authService.loadToken(); // Get token so it can be attached to headers
    // Headers configuration options
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json', // Format set to JSON
        'authorization': this.authService.authToken // Attach token
      })
    });
  }

  dbbDownload() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'version/dbbDownload', this.options).map(res => res.json());
  }

  scoresWordsDownload(username) {
    this.createAuthenticationHeaders();
    return this.http.post(this.domain + 'training/scoresWordsDownload', username, this.options).map(res => res.json());
  }

  // Function to ask the server the new training
  lastUpdateBd() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + 'version/lastUpdateBd', this.options).map(res => res.json());
  }

  sendScoretoDatabe(results, typeOfTraining) {
    let result;
    this.createAuthenticationHeaders();
    switch (typeOfTraining) {
      case 'vocabulary':
        result = this.http.post(this.domain + 'training/sendResultVocabulary', results, this.options).map(res => res.json());
        break;
      case 'plural':
        result = this.http.post(this.domain + 'training/sendResultPlural', results, this.options).map(res => res.json());
        break;
      case 'gender':
        result = this.http.post(this.domain + 'training/sendResultGender', results, this.options).map(res => res.json());
        break;
      case 'declinaison':
        result = this.http.post(this.domain + 'training/sendResultDeclinaison', results, this.options).map(res => res.json());
        break;
      case 'irregularVerbs':
        result = this.http.post(this.domain + 'training/sendResultIrregularVerbs', results, this.options).map(res => res.json());
        break;
      default:
    }
    return result;
  }

  sendResultSerietoDatabe(resultSerie) {
    let result;
    this.createAuthenticationHeaders();
    result = this.http.post(this.domain + 'training/sendResultSerie', resultSerie, this.options).map(res => res.json());
    return result;
  }


  scoresSeriesDownload(username) {
    this.createAuthenticationHeaders();
    return this.http.post(this.domain + 'training/scoresSeriesDownload', username, this.options).map(res => res.json());
  }


}
