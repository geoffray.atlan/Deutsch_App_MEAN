import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthService } from './auth.service';

@Injectable()
export class DashboardService {

  scoresSeries: IScoresSeries[];

  constructor(
    private http: Http,
    private authService: AuthService
  ) {}

}
