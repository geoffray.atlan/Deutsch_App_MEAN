import { TestBed, inject } from '@angular/core/testing';

import { SelectTrainingService } from './select-training.service';

describe('SelectTrainingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectTrainingService]
    });
  });

  it('should be created', inject([SelectTrainingService], (service: SelectTrainingService) => {
    expect(service).toBeTruthy();
  }));
});
