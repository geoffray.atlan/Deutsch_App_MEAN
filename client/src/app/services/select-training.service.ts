import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthService } from './auth.service';



@Injectable()
export class SelectTrainingService {

    // On déclare ici un objet vide qui viendra etre populé par le component select training
    // avec les caractéristiques de l'entrainement
    private messageSource = new BehaviorSubject<Object>([]);
    currentMessage = this.messageSource.asObservable();

    domain = this.authService.domain;
    options;

    constructor(
        private http: Http,
        private authService: AuthService
    ) {}

    changeMessage(message: Object) {
        this.messageSource.next(message);
    }

  // Function to create headers, add token, to be used in HTTP requests
  createAuthenticationHeaders() {
    this.authService.loadToken(); // Get token so it can be attached to headers
    // Headers configuration options
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json', // Format set to JSON
        'Authorization': this.authService.authToken // Attach token
      })
    });
  }
}
