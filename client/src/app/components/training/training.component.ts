import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { SelectTrainingService } from '../../services/select-training.service';
import { DbService } from '../../services/db.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})

export class TrainingComponent implements OnInit, OnDestroy {

  trainingCharacteristics: Object;
  typeOfTraining: String;
  levelOfTraining: String;
  orderOfTraining: String;
  vocabularyTypeOfTraining: String;
  repetitionNumber: number;
  repetitionNumber2: number;
  tipsNumber: number;
  tipsWord: string;
  username: string;
  progress: Number = 0;
  resultProgress: string;

  tipWord: string;
  tiptranslation: string;
  tipVInfinitif: string;
  tipVTraduction: string;

  isNoun: Boolean;
  isVerbs: Boolean;
  isDeclinaison: Boolean;
  responseAsked: Boolean;
  tipsAsked: Boolean;
  superLikeAsked: Boolean;

  results: IWord;
  vocabulary: IWord[];
  notation: {} = {};
  // notation: {[key: string]: INote} = {};

  wesh: Array<number> = [];
  wesh2: number = 0;
  resultSerieGrade: Number = 0;
  resultSerie: {} = {};

  date = new Date();

  constructor(
    private selectTraining: SelectTrainingService,
    private dbService: DbService,
    private http: Http,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.selectTraining.currentMessage.subscribe(trainingCharacteristics => {
      this.typeOfTraining = trainingCharacteristics['typeTraining'];
      this.levelOfTraining = trainingCharacteristics['levelTraining'];
      this.orderOfTraining = trainingCharacteristics['orderTraining'];
      this.vocabularyTypeOfTraining = trainingCharacteristics['vocabularyType'];
      this.repetitionNumber2 = this.repetitionNumber = trainingCharacteristics['repetitionNumber'];
      this.tipsNumber = 0;
      this.responseAsked = false;
      this.superLikeAsked = false;
      // console.log(this.orderOfTraining)
    });

    this.authService.getProfile().subscribe(profile => {
      this.username = profile.user.username;
    });

    const jsFilePath = this.selectTrainingLoad(this.typeOfTraining);  // Gather the required JSON file and launch the proper test
  }

  selectTrainingLoad(typeOfTraining) {
    let jsFilePath: string;
    switch (typeOfTraining) {
      case 'vocabulary':
        jsFilePath = 'data/words/training.json';
        this.nounTraining(this.levelOfTraining, this.vocabularyTypeOfTraining, jsFilePath, this.orderOfTraining);
        break;
      case 'plural':
        jsFilePath = 'data/words/training.json';
        this.orderOfTraining = 'Deutsch';
        this.nounTraining(this.levelOfTraining, 'Nom', jsFilePath, 'Deutsch');
        break;
      case 'gender':
        jsFilePath = 'data/words/training.json';
        this.orderOfTraining = 'Deutsch';
        this.nounTraining(this.levelOfTraining, 'Nom', jsFilePath, 'Deutsch');
        break;
      case 'declinaison':
        jsFilePath = 'data/words/declinaisons.json';
        this.declinaisonTraining(jsFilePath);
        break;
      case 'irregularVerbs':
        jsFilePath = 'data/words/irregularVerbs.json';
        this.verbTraining(this.orderOfTraining, jsFilePath);
        break;
      default:
    }
    return jsFilePath;
  }

  verbTraining(order, jsFilePath) {
    // Read the result field from the JSON response.
    this.http.get(jsFilePath).subscribe(selectTraining => {
      this.isVerbs = true;

      // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
      this.vocabulary = selectTraining.json();

      // Tant que le nombre de répétition n'est pas épuisé
      const randomInt = this.getRandomIntInclusive(0, this.vocabulary.length); // Creation of a random number
      this.results = this.vocabulary[randomInt];
    });
    this.isVerbs = false;
  }

  declinaisonTraining(jsFilePath) {
    // Read the result field from the JSON response.
    this.http.get(jsFilePath).subscribe(selectTraining => {
      this.isDeclinaison = true;

      // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
      this.vocabulary = selectTraining.json();

      // Tant que le nombre de répétition n'est pas épuisé
      const randomInt = this.getRandomIntInclusive(0, this.vocabulary.length); // Creation of a random number
      this.results = this.vocabulary[randomInt];
    });
    this.isDeclinaison = false;
  }

  nounTraining(level, wordType, jsFilePath, order) {
    // Read the result field from the JSON response.
    this.http.get(jsFilePath).subscribe(selectTraining => {
      this.isNoun = true;

      // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
      this.vocabulary = selectTraining.json()
      .filter(item => item['level'] === level || level === 'All')
      .filter(item => item['wordType'] === wordType || wordType === 'All');

      // console.log(this.vocabulary)
      // Tant que le nombre de répétition n'est pas épuisé
        const randomInt = this.getRandomIntInclusive(0, this.vocabulary.length); // Creation of a random number
        this.results = this.vocabulary[randomInt];

        // console.log(this.results)

        if (this.results['wordType'] === 'Nom') { // pour afficher plus facilement les déterminants.
            this.results['germanGender'] = this.genderDeutschDisplay(this.results['germanGender']);
            this.results['frenchGender'] = this.genderFrenchDisplay(this.results['frenchGender']);
        }
    });
    this.isNoun = false;
  }

  genderDeutschDisplay(determiner) {
    const allDeutschDeterminer = { 'M': 'Der', 'F': 'Die', 'N': 'Das', 'P': 'Die'};
    return allDeutschDeterminer[determiner]; // remplace un 'switch', en plus joli
  }

  genderFrenchDisplay(determiner) {
    const allFrenchDeterminer = { 'M': 'Un', 'F': 'Une', 'P': 'Des' };
    return allFrenchDeterminer[determiner]; // remplace un 'switch', en plus joli
  }

  showResponse() {
    this.responseAsked = true;
    this.tipsAsked = false;
  }

  displayNounTips() {
    if (this.responseAsked === true) {
      return;
    } else {
      if (this.tipsNumber < 3) {
      this.tipsAsked = true;
      ++this.tipsNumber;
      this.tipWord = this.results.word.substr(0, this.tipsNumber);
      this.tiptranslation = this.results.translation.substr(0, this.tipsNumber);
      } else {
        this.showResponse();
      }
    }
  }

  displayVerbsTips() {
    if (this.responseAsked === true) {
      return;
    } else {
      if (this.tipsNumber < 3) {
      this.tipsAsked = true;
      ++this.tipsNumber;
      this.tipVInfinitif = this.results['Infinitif'].substr(0, this.tipsNumber);
      this.tipVTraduction = this.results['translation'].substr(0, this.tipsNumber);
      } else {
        this.showResponse();
      }
    }
  }

  superLike() {
    if (this.superLikeAsked === true) {
      this.superLikeAsked = false;
    } else {
      this.superLikeAsked = true;
    }
  }

  get currentScore() {
    if (!(this.results.id in this.notation)) {
      this.notation[this.results.id] = {note: 0, superlike: this.superLikeAsked};
    }
    return this.notation[this.results.id]
  }

  reduceRepetition(wesh) {
    if (this.repetitionNumber > 0) {
      // Notation
      if (this.responseAsked === true) {
        this.currentScore.note += 0;
      } else if (this.responseAsked === false) {
        if (wesh === 'win') {
          if (this.tipsNumber === 0) {
            this.currentScore.note += 2;
          } else if (this.tipsNumber === 1) {
            this.currentScore.note += 1;
          } else if (this.tipsNumber >= 1) {
            this.currentScore.note += 0;
          }
        } else if (wesh === 'loose') {
          this.currentScore.note += 0;
        }
      }

      const jsFilePath = this.selectTrainingLoad(this.typeOfTraining);
      this.tipsAsked = false;
      this.tipsNumber = 0;
      this.responseAsked = false;
      // console.log(this.notation)
      this.repetitionNumber--;
      this.progressBar();
    } else {
      this.calculateGeneralScore(this.notation);
      this.sendScoretoDatabe(this.notation, this.typeOfTraining);
      this.router.navigate(['/']);
    }
  }

  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  calculateGeneralScore(notation){
    this.wesh = Object.values(notation).map(x => x.note)
    this.wesh2 = this.wesh.reduce((acc, res) => {
      return acc + res;
    })
    this.resultSerieGrade = this.wesh2 / this.repetitionNumber2;
  }

  sendScoretoDatabe(notation, typeOfTraining) {
    this.notation['username'] = this.username;
    this.dbService.sendScoretoDatabe(notation, typeOfTraining).subscribe(data => {
      // console.log(data);
    });

    this.resultSerie['resultSerieGrade'] = this.resultSerieGrade;
    this.resultSerie['typeOfTraining'] = this.typeOfTraining;
    this.resultSerie['date'] = this.date;
    this.resultSerie['username'] = this.username;

    this.dbService.sendResultSerietoDatabe(this.resultSerie).subscribe(data => {
      // console.log(data);
    });
  }

  progressBar(){
    this.progress = Math.round((this.repetitionNumber2 - (this.repetitionNumber)) * 100 / this.repetitionNumber2);
    // console.log(this.progress);
    this.resultProgress = this.progress + '%';
    return this.resultProgress;
  }

  ngOnDestroy() {
      this.typeOfTraining = '';
      this.levelOfTraining = '';
      this.orderOfTraining = '';
      this.vocabularyTypeOfTraining = '';
      this.notation = {};
  }
}
