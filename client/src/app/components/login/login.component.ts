import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { DbService } from '../../services/db.service';
import { SelectTrainingService } from '../../services/select-training.service';
import { AuthGuard } from '../../guards/auth.guard';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  messageClass;
  message;
  processing = false;
  form: FormGroup;
  previousUrl;
  username: {} = {};

  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    private dbService: DbService,
    private router: Router,
    private http: Http,
    private authGuard: AuthGuard,
    private selectTrainingService: SelectTrainingService,
    private dashboardService: DashboardService
  ) {
    this.createForm(); // Create Login Form when component is constructed
  }

  // Function to create login form
  createForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required], // Username field
      password: ['', Validators.required] // Password field
    });
  }

  // Function to disable form
  disableForm() {
    this.form.controls['username'].disable(); // Disable username field
    this.form.controls['password'].disable(); // Disable password field
  }

  // Function to enable form
  enableForm() {
    this.form.controls['username'].enable(); // Enable username field
    this.form.controls['password'].enable(); // Enable password field
  }

  // Functiont to submit form and login user
  onLoginSubmit() {
    this.processing = true; // Used to submit button while is being processed
    this.disableForm(); // Disable form while being process
    // Create user object from user's input
    const user = {
      username: this.form.get('username').value, // Username input field
      password: this.form.get('password').value // Password input field
    }

    // Function to send login data to API
    this.authService.login(user).subscribe(data => {
      // Check if response was a success or error
      if (!data.success) {
        this.messageClass = 'alert alert-danger'; // Set bootstrap error class
        this.message = data.message; // Set error message
        this.processing = false; // Enable submit button
        this.enableForm(); // Enable form for editting
      } else {
        this.messageClass = 'alert alert-success'; // Set bootstrap success class
        this.message = data.message; // Set success message
        // Function to store user's token in client local storage
        this.authService.storeUserData(data.token, data.user);
        // function to fetch the username Value
        this.authService.getProfile().subscribe(profile => {
          this.username['username'] = profile.user.username;
        });
        // After 2 seconds, redirect to dashboard page
        setTimeout(() => {

          // get the version number of the app in the jsonFile
          const trainingJson = 'data/words/version.json';
          this.http.get(trainingJson).subscribe(dataJson => {
            const trainingFile = dataJson.json();
            const versionJson = trainingFile[0].version;

            // get the version number of the app in the jsonFile
            this.dbService.lastUpdateBd().subscribe(dataDb => {
              // Response from registration attempt
              if (!dataDb.success) {
                this.messageClass = 'alert alert-danger'; // Set an error class
                this.message = dataDb.message; // Set an error message
                this.processing = false; // Re-enable submit button
                this.enableForm(); // Re-enable form
              } else {
                this.messageClass = 'alert alert-success'; // Set a success class
                this.message = dataDb.message; // Set a success message
                const versionMongo = this.message[0].version;

                // Load scores of every series
                this.dbService.scoresSeriesDownload(this.username).subscribe(scoresSeriesDownload => {
                  this.dashboardService.scoresSeries = scoresSeriesDownload.message;
                  // console.log(this.dashboardService.scoresSeries)
                });

                if(versionJson === versionMongo) {
                  console.log('les deux dates sont similaires');

                  // Load scores of every words
                  // this.dbService.scoresWordsDownload(this.username).subscribe(scoresWordsDownload => {
                    // localStorage.setItem('wordScores', JSON.stringify(scoresWordsDownload.message.wordScores));
                    // localStorage.setItem('pluralScores', JSON.stringify(scoresWordsDownload.message.pluralScores));
                    // localStorage.setItem('irregularVerbsScores', JSON.stringify(scoresWordsDownload.message.irregularVerbsScores));
                    // localStorage.setItem('genderScores', JSON.stringify(scoresWordsDownload.message.genderScores));
                    // localStorage.setItem('declinaisonScores', JSON.stringify(scoresWordsDownload.message.declinaisonScores));
                    // const retrievedObject = localStorage.getItem('wordScores');
                    // console.log(retrievedObject);
                    // const parsedObject = JSON.parse(retrievedObject);
                    // console.log(parsedObject);
                  // });

                } else {
                  // this.dbService.dbbDownload().subscribe(dbbDownload => {
                  //   localStorage.setItem('trainingVocabulary', JSON.stringify(dbbDownload.message));
                  //   const retrievedObject = localStorage.getItem('trainingVocabulary');
                  //   const parsedObject = JSON.parse(retrievedObject);
                  //   // console.log(parsedObject[0].word);
                  // });
                }

                // After 2 second timeout, navigate to the login page
                setTimeout(() => {
                  this.router.navigate(['/login']); // Redirect to login view
                }, 2000);
              }
            });

          });
          // Check if user was redirected or logging in for first time
          if (this.previousUrl) {
            this.router.navigate([this.previousUrl]); // Redirect to page they were trying to view before
          } else {
            this.router.navigate(['/']); // Navigate to dashboard view
          }
        }, 500);
      }
    });
  }

  ngOnInit() {
    // On page load, check if user was redirected to login
    if (this.authGuard.redirectUrl) {
      this.messageClass = 'alert alert-danger'; // Set error message: need to login
      this.message = 'You must be logged in to view that page.'; // Set message
      this.previousUrl = this.authGuard.redirectUrl; // Set the previous URL user was redirected from
      this.authGuard.redirectUrl = undefined; // Erase previous URL
    }
  }

}
