import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { SelectTraining } from './select-training';
import { AuthService } from '../../services/auth.service';
import { SelectTrainingService } from '../../services/select-training.service';


@Component({
  selector: 'app-select-training',
  templateUrl: './select-training.component.html',
  styleUrls: ['./select-training.component.css']
})
export class SelectTrainingComponent implements OnInit {

  private trainingsType: Array <string>;
  private trainingsLevel: Array <string>;
  private trainingsOrder: Array <string>;
  private trainingVocabularyType: Array <string>;

  messageClass;
  message;
  username;

  canChooseType: Boolean;
  canChooseLevel: Boolean;
  canChooseOrder: Boolean;
  canChooseVocabulary: Boolean;

  // Results !
  typeTraining: String;
  levelTraining: String;
  orderTraining: String;
  vocabularyType: String;
  repetitionNumber: Number;
  selectedTraining: Object;

  constructor(
    private router: Router,
    public authService: AuthService,
    private selectTraining: SelectTrainingService
  ) {
    this.trainingsType = ['vocabulary',
                          'declinaison',
                          'gender',
                          'irregularVerbs',
                          'plural'
                          ];
    this.trainingsLevel = ['All',
                          'A21',
                          'A22',
                          'B11',
                          'B12'
                          ];
    this.trainingsOrder = ['Deutsch',
                          'Français'
                          ];
    this.trainingVocabularyType = ['All',
                          'Verbe',
                          'Nom',
                          'Expression',
                          'Adverbe',
                          'Adjectif',
                          'Preposition',
                          'Pronom'
                          ];
  }

  selectTypeTraining(event, typeOfTraining) {
    this.typeTraining = typeOfTraining;

    // Choix supplémentaire pour les types d'entrainements ci-dessous
    if (this.typeTraining === 'vocabulary' ||
       this.typeTraining === 'gender' ||
       this.typeTraining === 'plural') {
      this.canChooseLevel = true;
    } else {
      this.canChooseLevel = false;
    }

    // Option supplémentaire pour ce type d'entrainement
    if (this.typeTraining === 'irregularVerbs') {
      this.canChooseOrder = true;
    }

    // Création de l'objet dans le cas des déclinaisons
    if (this.typeTraining === 'declinaison') {
        this.createObject();
    }

    // Sinon
    this.canChooseType = false;
  }

  selectLevelTraining(event, levelOfTraining) {
    this.levelTraining = levelOfTraining;

    // Choix supplémentaire pour les types d'entrainements ci-dessous
    if (this.typeTraining === 'vocabulary') {
      this.canChooseVocabulary = true;
    } else {
      this.canChooseVocabulary = false;
    }

    // Création de l'objet dans le cas du plural et du gender
    if (this.typeTraining === 'plural' || this.typeTraining === 'gender') {
      this.createObject();
    }
    this.canChooseLevel = false;
  }

  selectVocabularyType(event, typeOfVocabulary) {
    this.vocabularyType = typeOfVocabulary;

    // Choix supplémentaire pour les types d'entrainements ci-dessous
    if (this.typeTraining === 'vocabulary') {
      this.canChooseOrder = true;
    } else {
      this.canChooseOrder = false;
    }

    this.canChooseVocabulary = false;
  }

  selectOrderTraining(event, orderOfTraining) {
    this.orderTraining = orderOfTraining;

    // Création de l'objet pour le vocabulary
    if (this.typeTraining === 'vocabulary') {
      this.createObject();
    }

    // Création de l'objet pour les Verbes Irréguliers
    if (this.typeTraining === 'irregularVerbs') {
      this.createObject();
    }

    this.canChooseOrder = false;
    // direction vers le module d'entrainement
  }

  // Cette fonction est appelée à la fin de chaque sélection afin de créer
  // un objet qui contiendra toutes les caractéristiques de l'entrainement.
  // Envoi de celles-ci par le biais du data service. Esh
  createObject() {
    this.selectedTraining = new SelectTraining(this.typeTraining,
                                              this.levelTraining,
                                              this.orderTraining,
                                              this.vocabularyType,
                                              this.repetitionNumber);
    this.selectTraining.changeMessage(this.selectedTraining);
    this.router.navigate(['/training']);
  }


  ngOnInit() {
    this.typeTraining = null;
    this.levelTraining = null;
    this.orderTraining = null;
    this.vocabularyType = null;
    this.repetitionNumber = 15;

    this.canChooseType = true;
    this.canChooseLevel = false;
    this.canChooseOrder = false;
    this.canChooseVocabulary = false;
  }

}
