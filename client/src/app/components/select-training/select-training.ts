export class SelectTraining {
    constructor(
        private typeTraining: String,
        private levelTraining: String,
        private orderTraining: String,
        private vocabularyType: String,
        private repetitionNumber: Number
    ) { }

    getTypeTraining() {
        return this.typeTraining;
    }

    geLevelTraining() {
        return this.levelTraining;
    }

    getOrderTraining() {
        return this.orderTraining;
    }

    getVocabularyType() {
        return this.vocabularyType;
    }

    getRepetitionNumber() {
        return this.repetitionNumber;
    }
  }
