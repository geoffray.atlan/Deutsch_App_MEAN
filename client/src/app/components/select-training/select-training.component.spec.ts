import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTrainingComponent } from './select-training.component';

describe('SelectTrainingComponent', () => {
  let component: SelectTrainingComponent;
  let fixture: ComponentFixture<SelectTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
