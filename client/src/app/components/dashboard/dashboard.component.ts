import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DashboardService } from '../../services/dashboard.service';

declare var Plotly: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input() data: any;
  @Input() layout: any;
  @Input() options: any;
  @Input() displayRawData: boolean;

  scoresSeries: IScoresSeries[];
  arrayDate : Array<Date> = [];
  arrayTypeTraining : Array<String> = [];
  arrayResultTraining : Array<Number> = [];

  minute: number;
  hour: number;
  day: number;
  month: number;
  year: number;
  date: Date;

  name: string;
  PlotlyLayout: any;
  PlotlyData: any;
  PlotlyOptions: any;

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    // console.log(this.dashboardService.scoresSeries)
    this.getScores();
    // console.log(this.arrayDate);
    // console.log(this.arrayTypeTraining);
    // console.log(this.arrayResultTraining);
  }


  getScores() {

    // First, we create the arrays containing the final data
    this.dashboardService.scoresSeries.map( x => {
      // conversion of the string in Date in order to sort it in the graph
      this.minute = parseInt(x.date.substring(14, 16), 10);
      this.hour = parseInt(x.date.substring(11, 13), 10);
      this.day = parseInt(x.date.substring(8, 10), 10);
      this.month = parseInt(x.date.substring(5, 7), 10) - 1;
      this.year = parseInt(x.date.substring(0, 4), 10);

      this.date = new Date(this.year, this.month, this.day, this.hour, this.minute);

      this.arrayDate.push(this.date)
      this.arrayTypeTraining.push(x.typeOfTraining)
      this.arrayResultTraining.push(x.resultSerieGrade * 100 * 0.5)
    });

    // Second, we order those data correctly for the display.
    this.dataDisplayBar();
    this.dataDisplayLine();

  }

  dataDisplayBar(){

    this.PlotlyLayout = {
        title: 'Result of every trainings'
        // height: 500,
        // width: 500
    };

    this.PlotlyData = [
        {
          x: this.arrayDate,
          y: this.arrayResultTraining,
          type: 'bar',
          orientation: 'v'
        }
      ];

      Plotly.newPlot('myPlotlyDivBar', this.PlotlyData, this.PlotlyLayout, this.options);
  }


  dataDisplayLine(){

    let trace1 = {
      x: [1, 2, 3, 4, 5],
      y: this.arrayResultTraining.slice(Math.max(this.arrayResultTraining.length - 5, 0)),
      mode: 'lines+markers'
    };

    let layout = {
      title:'Result of the last five trainings'
    };

    let data = [trace1];

    Plotly.newPlot('myPlotlyDivLine', data, layout);
  }

}