
// interface INote{
//     note:number;
//     superlike?:Boolean;
// }

interface IUser{
    email: string;
    username:  string;
    password: string;
    wordScores: { [key:string]: {note:number; superlike?:Boolean;},
    // wordScores: { [key:string]: INote },
}}
interface IWord{
    id: number,
    word: String,
    germanVerbPreposition: String,
    germanGender: String,
    translation: String,
    frenchGender: String,
    wordType: String,
    wordPlural: String,
    lesson: Number,
    level: String,
  }

  interface IScoresSeries {
    date: String;
    typeOfTraining: String;
    resultSerieGrade: number;
}
