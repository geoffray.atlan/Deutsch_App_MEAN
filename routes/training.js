const User = require('../models/user'); // Import User Model Schema
const Training = require('../models/training'); // Import Training Model Schema
const jwt = require('jsonwebtoken'); // Compact, URL-safe means of representing claims to be transferred between two parties.
const config = require('../config/database'); // Import database configuration

module.exports = (router) => {

  router.post('/newtraining', (req, res) => {
    if (!req.body.typeTraining) {
      res.json({ success: false, message: 'type of training is required' });
    } else {
      if (!req.body.levelTraining) {
        res.json({ success: false, message: 'level of training is required' })
      } else {
        if (!req.body.orderTraining) {
          res.json({ success: false, message: 'order of training is required' })
        } else {
          if (!req.body.vocabularyType) {
            res.json({ success: false, message: 'type of vocabulary is required' })
          } else {
            if (!req.body.repetitionNumber) {
              res.json({ success: false, message: 'number of repetition is required' })
            } else {
              const training = new Training({
                typeTraining: req.body.typeTraining,
                levelTraining: req.body.levelTraining,
                orderTraining: req.body.orderTraining,
                vocabularyType: req.body.vocabularyType,
                repetitionNumber: req.body.repetitionNumber
              });
              training.save();
            }
          }
        }
      }
    }
  });

  router.post('/sendResultVocabulary', (req, res) => {

    let resultUser = [];
    let username = req.body.username;
    let resultFromTest = req.body;
    delete resultFromTest['username']; // remove the element 'username' ot the array

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
          // Permet de retourner un tableau sous la forme : [ { note: 2, id: '200' }, ... suivant le nombre de répétition]
          resultFromTest  =  Object.keys(resultFromTest).map(x=>{ resultFromTest[x].id=x; return resultFromTest[x];});
          let counter = resultFromTest.length-1;

          while(counter >= 0){
            // If the word is not in the database, we add the value to the [id] position of the resultUser table
            if(!(resultFromTest[counter].id in resultUser) || resultUser[resultFromTest[counter].id] == null){
              resultUser[resultFromTest[counter].id] = {note: resultFromTest[counter].note, times: 1, superlike: resultFromTest[counter].superlike}
              
            // If the word is already in the database, we add the value to the last one
            } else if(resultUser[resultFromTest[counter].id] != null){
              resultUser[resultFromTest[counter].id] = {note: (resultFromTest[counter].note + resultUser[resultFromTest[counter].id].note), times: (resultUser[resultFromTest[counter].id].times + 1), superlike: resultFromTest[counter].superlike};
            }
            // console.log(resultFromTest[counter].id, resultUser[resultFromTest[counter].id].times, resultUser[resultFromTest[counter].id].note, resultFromTest[counter].superlike)

            counter--;
          }

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {wordScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/sendResultPlural', (req, res) => {
    
    let resultUser = [];
    let username = req.body.username;
    let resultFromTest = req.body;
    delete resultFromTest['username']; // remove the element 'username' ot the array

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        resultUser = result.wordScores
  
        // Permet de retourner un tableau sous la forme : [ { note: 2, id: '200' }, ... suivant le nombre de répétition]
        resultFromTest  =  Object.keys(resultFromTest).map(x=>{ resultFromTest[x].id=x; return resultFromTest[x];});
        let counter = resultFromTest.length-1;


        while(counter >= 0){
          // If the word is not in the database, we add the value to the [id] position of the resultUser table
          if(!(resultFromTest[counter].id in resultUser) || resultUser[resultFromTest[counter].id] == null){
            resultUser[resultFromTest[counter].id] = {note: resultFromTest[counter].note, times: 1}
            // If the word is already in the database, we add the value to the last one
          } else if(resultUser[resultFromTest[counter].id] != null){
            resultUser[resultFromTest[counter].id] = {note: (resultFromTest[counter].note + resultUser[resultFromTest[counter].id].note), times: (resultUser[resultFromTest[counter].id].times + 1)};
          }
          // console.log(resultFromTest[counter].id, resultUser[resultFromTest[counter].id].times, resultUser[resultFromTest[counter].id].note)
          
          counter--;
        }

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {wordScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/sendResultGender', (req, res) => {

    let resultUser = [];
    let username = req.body.username;
    let resultFromTest = req.body;
    delete resultFromTest['username']; // remove the element 'username' ot the array

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        resultUser = result.wordScores
    
        // Permet de retourner un tableau sous la forme : [ { note: 2, id: '200' }, ... suivant le nombre de répétition]
        resultFromTest  =  Object.keys(resultFromTest).map(x=>{ resultFromTest[x].id=x; return resultFromTest[x];});
        let counter = resultFromTest.length-1;

        while(counter >= 0){
          // If the word is not in the database, we add the value to the [id] position of the resultUser table
          if(!(resultFromTest[counter].id in resultUser) || resultUser[resultFromTest[counter].id] == null){
            resultUser[resultFromTest[counter].id] = {note: resultFromTest[counter].note, times: 1}
            // If the word is already in the database, we add the value to the last one
          } else if(resultUser[resultFromTest[counter].id] != null){
            resultUser[resultFromTest[counter].id] = {note: (resultFromTest[counter].note + resultUser[resultFromTest[counter].id].note), times: (resultUser[resultFromTest[counter].id].times + 1)};
          }
          // console.log(resultFromTest[counter].id, resultUser[resultFromTest[counter].id].times, resultUser[resultFromTest[counter].id].note)
          
          counter--;
        }

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {wordScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/sendResultDeclinaison', (req, res) => {
  
    let resultUser = [];
    let username = req.body.username;
    let resultFromTest = req.body;
    delete resultFromTest['username']; // remove the element 'username' ot the array

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        resultUser = result.wordScores
    
        // Permet de retourner un tableau sous la forme : [ { note: 2, id: '200' }, ... suivant le nombre de répétition]
        resultFromTest  =  Object.keys(resultFromTest).map(x=>{ resultFromTest[x].id=x; return resultFromTest[x];});
        let counter = resultFromTest.length-1;

        while(counter >= 0){
          // If the word is not in the database, we add the value to the [id] position of the resultUser table
          if(!(resultFromTest[counter].id in resultUser) || resultUser[resultFromTest[counter].id] == null){
            resultUser[resultFromTest[counter].id] = {note: resultFromTest[counter].note, times: 1}
            // If the word is already in the database, we add the value to the last one
          } else if(resultUser[resultFromTest[counter].id] != null){
            resultUser[resultFromTest[counter].id] = {note: (resultFromTest[counter].note + resultUser[resultFromTest[counter].id].note), times: (resultUser[resultFromTest[counter].id].times + 1)};
          }
          // console.log(resultFromTest[counter].id, resultUser[resultFromTest[counter].id].times, resultUser[resultFromTest[counter].id].note)
          
          counter--;
        }

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {wordScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/sendResultIrregularVerbs', (req, res) => {
    
    let resultUser = [];
    let username = req.body.username;
    let resultFromTest = req.body;
    delete resultFromTest['username']; // remove the element 'username' ot the array

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        resultUser = result.wordScores
    
          // Permet de retourner un tableau sous la forme : [ { note: 2, id: '200' }, ... suivant le nombre de répétition]
          resultFromTest  =  Object.keys(resultFromTest).map(x=>{ resultFromTest[x].id=x; return resultFromTest[x];});
          let counter = resultFromTest.length-1;


          while(counter >= 0){
            // If the word is not in the database, we add the value to the [id] position of the resultUser table
            if(!(resultFromTest[counter].id in resultUser) || resultUser[resultFromTest[counter].id] == null){
              resultUser[resultFromTest[counter].id] = {note: resultFromTest[counter].note, times: 1}
              // If the word is already in the database, we add the value to the last one
            } else if(resultUser[resultFromTest[counter].id] != null){
              resultUser[resultFromTest[counter].id] = {note: (resultFromTest[counter].note + resultUser[resultFromTest[counter].id].note), times: (resultUser[resultFromTest[counter].id].times + 1)};
            }
            // console.log(resultFromTest[counter].id, resultUser[resultFromTest[counter].id].times, resultUser[resultFromTest[counter].id].note)
            
            counter--;
          }

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {wordScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/sendResultSerie', (req, res) => {  // Permet d'ajouter les scores obtenus à chaque série pour l'user.

    let resultUser = [];
    let username = req.body.username;
    let resultSerieGrade = req.body.resultSerieGrade
    let typeOfTraining = req.body.typeOfTraining
    let date = req.body.date

    User.findOne({username: username}, (err, result) => { // injecter les score ici !!!!!!!!
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {

        resultUser = result.seriesScores

        resultUser.push({typeOfTraining : typeOfTraining, resultSerieGrade : resultSerieGrade, date : date})

        // Then, we update the Db with the previous scores updated with the new results
        User.update({username: username}, {$set : {seriesScores: resultUser}}, (err, result) => { // injecter les score ici !!!!!!!!
          if (err) {
            res.json({ success: false, message: err }); // Return connection error
          } else {
            res.json({ success: true, message: result }); // Return as vailable username
          }
        })
      }
    })
  });

  router.post('/scoresWordsDownload', (req, res) => {

    let username = req.body.username;
    let scoresArray = {};

    User.find({username: username}, (err, result) => {
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        // scoresArray['pluralScores'] = result[0].pluralScores
        scoresArray['pluralScores'] = result[0].pluralScores
        scoresArray['irregularVerbsScores'] = result[0].irregularVerbsScores
        scoresArray['genderScores'] = result[0].genderScores
        scoresArray['declinaisonScores'] = result[0].declinaisonScores
        scoresArray['wordScores'] = result[0].wordScores
        res.json({ success: true, message: scoresArray }); // Return as vailable username
      }
    })
  });


  router.post('/scoresSeriesDownload', (req, res) => {

    let username = req.body.username;

    User.find({username: username}, (err, result) => {
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        res.json({ success: true, message: result[0].seriesScores }); // Return as vailable username
      }
    })
  });

  return router; // Return router object to main index.js
}
