const User = require('../models/user'); // Import User Model Schema
const Training = require('../models/training'); // Import Training Model Schema
const Version = require('../models/version'); // Import Training Model Schema
const jwt = require('jsonwebtoken'); // Compact, URL-safe means of representing claims to be transferred between two parties.
const config = require('../config/database'); // Import database configuration

module.exports = (router) => {


  /* ================================================
  During the login, check the validity of the DBB
  ================================================ */
  
  router.get('/lastUpdateBd', (req, res) => {
    Version.find({}, (err, result) => {
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {  
        res.json({ success: true, message: result }); // Return as vailable username
      }
    })
  });

  router.get('/dbbDownload', (req, res) => {
    Training.find({}, (err, result) => {
      if (err) {
        res.json({ success: false, message: err }); // Return connection error
      } else {
        res.json({ success: true, message: result }); // Return as vailable username
      }
    })
  });

  return router; // Return router object to main index.js
}
