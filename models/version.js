/* ===================
   Import Node Modules
=================== */
const mongoose = require('mongoose'); // Node Tool for MongoDB
mongoose.Promise = global.Promise; // Configure Mongoose Promises
const Schema = mongoose.Schema; // Import Schema from Mongoose

const versionSchema = new Schema({
  version: { type: Number },
  versionTraining: { type: Number },
  versionDeclinaison: { type: Number },
});

// Export Module/Schema
module.exports = mongoose.model('version', versionSchema);
