/* ===================
   Import Node Modules
=================== */
const mongoose = require('mongoose'); // Node Tool for MongoDB
mongoose.Promise = global.Promise; // Configure Mongoose Promises
const Schema = mongoose.Schema; // Import Schema from Mongoose

const trainingSchema = new Schema({
  id: { type: Number },
  word: { type: String },
  germanVerbPreposition: { type: String },
  germanGender: { type: String },
  translation: { type: String },
  frenchGender: { type: String },
  wordType: { type: String },
  wordPlural: { type: String },
  lesson: { type: Number },
  level: { type: String },
});

// Export Module/Schema
module.exports = mongoose.model('training', trainingSchema);


