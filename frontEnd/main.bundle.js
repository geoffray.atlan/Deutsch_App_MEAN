webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_profile_profile_component__ = __webpack_require__("../../../../../src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_lecon_lecon_component__ = __webpack_require__("../../../../../src/app/components/lecon/lecon.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_game_game_component__ = __webpack_require__("../../../../../src/app/components/game/game.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_training_training_component__ = __webpack_require__("../../../../../src/app/components/training/training.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_select_training_select_training_component__ = __webpack_require__("../../../../../src/app/components/select-training/select-training.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__guards_notAuth_guard__ = __webpack_require__("../../../../../src/app/guards/notAuth.guard.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













// Our Array of Angular 2 Routes
var appRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__components_home_home_component__["a" /* HomeComponent */] // Default Route
    },
    {
        path: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_3__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'game',
        component: __WEBPACK_IMPORTED_MODULE_8__components_game_game_component__["a" /* GameComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'lecon',
        component: __WEBPACK_IMPORTED_MODULE_7__components_lecon_lecon_component__["a" /* LeconComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_5__components_login_login_component__["a" /* LoginComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_12__guards_notAuth_guard__["a" /* NotAuthGuard */]] // User must NOT be logged in to view this route
    },
    {
        path: 'profile',
        component: __WEBPACK_IMPORTED_MODULE_6__components_profile_profile_component__["a" /* ProfileComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'training',
        component: __WEBPACK_IMPORTED_MODULE_9__components_training_training_component__["a" /* TrainingComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'select-training',
        component: __WEBPACK_IMPORTED_MODULE_10__components_select_training_select_training_component__["a" /* SelectTrainingComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* AuthGuard */]] // User must be logged in to view this route
    },
    {
        path: 'register',
        component: __WEBPACK_IMPORTED_MODULE_4__components_register_register_component__["a" /* RegisterComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_12__guards_notAuth_guard__["a" /* NotAuthGuard */]] // User must NOT be logged in to view this route
    },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_2__components_home_home_component__["a" /* HomeComponent */] } // "Catch-All" Route
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [],
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(appRoutes)],
            providers: [],
            bootstrap: [],
            exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<div class=\"container\">\n  <flash-messages></flash-messages>\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Hello World from Angular 2!';
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_profile_profile_component__ = __webpack_require__("../../../../../src/app/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_game_game_component__ = __webpack_require__("../../../../../src/app/components/game/game.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_training_training_component__ = __webpack_require__("../../../../../src/app/components/training/training.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_lecon_lecon_component__ = __webpack_require__("../../../../../src/app/components/lecon/lecon.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_select_training_select_training_component__ = __webpack_require__("../../../../../src/app/components/select-training/select-training.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__guards_notAuth_guard__ = __webpack_require__("../../../../../src/app/guards/notAuth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_select_training_service__ = __webpack_require__("../../../../../src/app/services/select-training.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_db_service__ = __webpack_require__("../../../../../src/app/services/db.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_dashboard_service__ = __webpack_require__("../../../../../src/app/services/dashboard.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_game_game_component__["a" /* GameComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_training_training_component__["a" /* TrainingComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_lecon_lecon_component__["a" /* LeconComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_select_training_select_training_component__["a" /* SelectTrainingComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4_angular2_flash_messages__["FlashMessagesModule"]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_19__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_17__guards_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_18__guards_notAuth_guard__["a" /* NotAuthGuard */],
                __WEBPACK_IMPORTED_MODULE_20__services_select_training_service__["a" /* SelectTrainingService */],
                __WEBPACK_IMPORTED_MODULE_21__services_db_service__["a" /* DbService */],
                __WEBPACK_IMPORTED_MODULE_22__services_dashboard_service__["a" /* DashboardService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/app.module.js.map

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#welcomingPhrase{\n    margin-top: 10%;\n    color: #777;\n    font-size: 125%;\n    text-align: center;\n}\n\n#myPlotlyDiv {\n    width: 85%;\n    margin: 10% 0% 10% 0%;\n    /* display: flex;\n    align-items: center;\n    justify-content: space-around;\n    flex-direction: column; */\n}\n\n\n@media screen and (max-width: 500px) {\n    #myPlotlyDiv {\n        width: 85%;\n        margin: 10% 0% 10% 0%;\n        /* display: flex;\n        align-items: center;\n        justify-content: space-around;\n        flex-direction: column; */\n    }\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<p id=\"welcomingPhrase\">Let see if your worked well</p>\n\n\n\n<div id=\"myPlotlyDivBar\"\n    name=\"myPlotlyDivBar\">\n<!-- Plotly chart will be drawn inside this DIV -->\n</div>\n\n<br><br><br>\n\n\n<div id=\"myPlotlyDivLine\"\n    name=\"myPlotlyDivLine\">\n<!-- Plotly chart will be drawn inside this DIV -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__ = __webpack_require__("../../../../../src/app/services/dashboard.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = (function () {
    function DashboardComponent(dashboardService) {
        this.dashboardService = dashboardService;
        this.arrayDate = [];
        this.arrayTypeTraining = [];
        this.arrayResultTraining = [];
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // console.log(this.dashboardService.scoresSeries)
        this.getScores();
        // console.log(this.arrayDate);
        // console.log(this.arrayTypeTraining);
        // console.log(this.arrayResultTraining);
    };
    DashboardComponent.prototype.getScores = function () {
        var _this = this;
        // First, we create the arrays containing the final data
        this.dashboardService.scoresSeries.map(function (x) {
            // conversion of the string in Date in order to sort it in the graph
            _this.minute = parseInt(x.date.substring(14, 16), 10);
            _this.hour = parseInt(x.date.substring(11, 13), 10);
            _this.day = parseInt(x.date.substring(8, 10), 10);
            _this.month = parseInt(x.date.substring(5, 7), 10) - 1;
            _this.year = parseInt(x.date.substring(0, 4), 10);
            _this.date = new Date(_this.year, _this.month, _this.day, _this.hour, _this.minute);
            _this.arrayDate.push(_this.date);
            _this.arrayTypeTraining.push(x.typeOfTraining);
            _this.arrayResultTraining.push(x.resultSerieGrade * 100 * 0.5);
        });
        // Second, we order those data correctly for the display.
        this.dataDisplayBar();
        this.dataDisplayLine();
    };
    DashboardComponent.prototype.dataDisplayBar = function () {
        this.PlotlyLayout = {
            title: 'Result of every trainings'
        };
        this.PlotlyData = [
            {
                x: this.arrayDate,
                y: this.arrayResultTraining,
                type: 'bar',
                orientation: 'v'
            }
        ];
        Plotly.newPlot('myPlotlyDivBar', this.PlotlyData, this.PlotlyLayout, this.options);
    };
    DashboardComponent.prototype.dataDisplayLine = function () {
        var trace1 = {
            x: [1, 2, 3, 4, 5],
            y: this.arrayResultTraining.slice(Math.max(this.arrayResultTraining.length - 5, 0)),
            mode: 'lines+markers'
        };
        var layout = {
            title: 'Result of the last five trainings'
        };
        var data = [trace1];
        Plotly.newPlot('myPlotlyDivLine', data, layout);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
        __metadata('design:type', Object)
    ], DashboardComponent.prototype, "data", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
        __metadata('design:type', Object)
    ], DashboardComponent.prototype, "layout", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
        __metadata('design:type', Object)
    ], DashboardComponent.prototype, "options", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
        __metadata('design:type', Boolean)
    ], DashboardComponent.prototype, "displayRawData", void 0);
    DashboardComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__["a" /* DashboardService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_dashboard_service__["a" /* DashboardService */]) === 'function' && _a) || Object])
    ], DashboardComponent);
    return DashboardComponent;
    var _a;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/game/game.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/game/game.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  game works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/game/game.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GameComponent = (function () {
    function GameComponent() {
    }
    GameComponent.prototype.ngOnInit = function () {
    };
    GameComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-game',
            template: __webpack_require__("../../../../../src/app/components/game/game.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/game/game.component.css")]
        }), 
        __metadata('design:paramtypes', [])
    ], GameComponent);
    return GameComponent;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/game.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* #boutonEntrainement, #profil, #boutonGame{\n    margin: 18px 10px 0px 10px;\n    width:180px;\n    font-size: 1.1em;\n} */\n\n\n.jumbotron {\n    margin-top: 10%;\n    padding: 2rem 1rem;\n    margin-bottom: 2rem;\n    background-color: #e9ecef;\n    border-radius: .3rem;\n}\n\n\nbutton{\n    margin: 13px 10px 0px 10px;\n    width:220px;\n    color: #777;\n    /* font-size: 1.1em; */\n    text-align: center;\n}\n\n\n\n#selector {\n    /* height: 85%; */\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    margin: 10% 0% 10% 0%;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!authService.loggedIn()\" class=\"jumbotron text-center\">\n  <h1>Magic German</h1>\n  <p class=\"lead\">Welcome to the best Application you ever hold.<strong> Be prepared !!</strong></p>\n  <div>\n    <a routerLink=\"/register\" class=\"btn btn-primary\">Sign up</a>\n    <a routerLink=\"/login\" class=\"btn btn-default\">log in</a>\n  </div>\n</div>\n\n<div *ngIf=\"authService.loggedIn()\" id=\"selector\" jumbotron text-center>\n  <button id=\"trainingButton\" type=\"button\" class=\"btn btn-default btn-lg\" routerLink=\"/select-training\">Training</button>\n  <button id=\"dashboardButton\" type=\"button\" class=\"btn btn-default btn-lg\" routerLink=\"/dashboard\">Dashboard</button>\n  <!-- <button id=\"gamesButton\" type=\"button\" class=\"btn btn-default btn-lg\" routerLink=\"/game\">Games</button> -->\n  <button id=\"leconButton\" type=\"button\" class=\"btn btn-default btn-lg\" routerLink=\"/lecon\">Lessons</button>\n  <button id=\"profilButton\" type=\"button\" class=\"btn btn-default btn-lg\" routerLink=\"/profile\">My profile</button>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = (function () {
    function HomeComponent(authService) {
        this.authService = authService;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === 'function' && _a) || Object])
    ], HomeComponent);
    return HomeComponent;
    var _a;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/home.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/lecon/lecon.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/lecon/lecon.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  lecon works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/lecon/lecon.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeconComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LeconComponent = (function () {
    function LeconComponent() {
    }
    LeconComponent.prototype.ngOnInit = function () {
    };
    LeconComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-lecon',
            template: __webpack_require__("../../../../../src/app/components/lecon/lecon.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/lecon/lecon.component.css")]
        }), 
        __metadata('design:paramtypes', [])
    ], LeconComponent);
    return LeconComponent;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/lecon.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Login</h2>\n\n<!-- Custom Success/Error Message -->\n<div class=\"row show-hide-message\">\n  <div [ngClass]=\"messageClass\">\n    {{ message }}\n  </div>\n</div>\n\n<!-- Login Form -->\n<form [formGroup]=\"form\" (submit)=\"onLoginSubmit()\">\n\n  <!-- Username Field -->\n  <div class=\"form-group\">\n    <label for=\"username\">Username</label>\n    <div [ngClass]=\"{'has-error': form.controls.username.errors && form.controls.username.dirty, 'has-success': form.controls.username.valid && form.controls.username.dirty }\">\n      <input class=\"form-control\" type=\"text\" name=\"username\" formControlName=\"username\" />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.username.errors?.required && form.controls.username.dirty\">This field is required.</li>\n      </ul>\n    </div>\n  </div>\n\n  <!-- Password Field  -->\n  <div class=\"form-group\">\n    <label for=\"password\">Password</label>\n    <div [ngClass]=\"{'has-error': form.controls.password.errors && form.controls.password.dirty, 'has-success': form.controls.password.valid && form.controls.password.dirty }\">\n      <input class=\"form-control\" type=\"password\" name=\"password\" formControlName=\"password\" />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.password.errors?.required && form.controls.password.dirty\">This field is required.</li>\n      </ul>\n    </div>\n  </div>\n  <!-- Submit Button -->\n  <input [disabled]=\"!form.valid || processing\" class=\"btn btn-primary\" type=\"submit\" value=\"Login\" />\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_db_service__ = __webpack_require__("../../../../../src/app/services/db.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_select_training_service__ = __webpack_require__("../../../../../src/app/services/select-training.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_dashboard_service__ = __webpack_require__("../../../../../src/app/services/dashboard.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginComponent = (function () {
    function LoginComponent(formBuilder, authService, dbService, router, http, authGuard, selectTrainingService, dashboardService) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.dbService = dbService;
        this.router = router;
        this.http = http;
        this.authGuard = authGuard;
        this.selectTrainingService = selectTrainingService;
        this.dashboardService = dashboardService;
        this.processing = false;
        this.username = {};
        this.createForm(); // Create Login Form when component is constructed
    }
    // Function to create login form
    LoginComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required] // Password field
        });
    };
    // Function to disable form
    LoginComponent.prototype.disableForm = function () {
        this.form.controls['username'].disable(); // Disable username field
        this.form.controls['password'].disable(); // Disable password field
    };
    // Function to enable form
    LoginComponent.prototype.enableForm = function () {
        this.form.controls['username'].enable(); // Enable username field
        this.form.controls['password'].enable(); // Enable password field
    };
    // Functiont to submit form and login user
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        this.processing = true; // Used to submit button while is being processed
        this.disableForm(); // Disable form while being process
        // Create user object from user's input
        var user = {
            username: this.form.get('username').value,
            password: this.form.get('password').value // Password input field
        };
        // Function to send login data to API
        this.authService.login(user).subscribe(function (data) {
            // Check if response was a success or error
            if (!data.success) {
                _this.messageClass = 'alert alert-danger'; // Set bootstrap error class
                _this.message = data.message; // Set error message
                _this.processing = false; // Enable submit button
                _this.enableForm(); // Enable form for editting
            }
            else {
                _this.messageClass = 'alert alert-success'; // Set bootstrap success class
                _this.message = data.message; // Set success message
                // Function to store user's token in client local storage
                _this.authService.storeUserData(data.token, data.user);
                // function to fetch the username Value
                _this.authService.getProfile().subscribe(function (profile) {
                    _this.username['username'] = profile.user.username;
                });
                // After 2 seconds, redirect to dashboard page
                setTimeout(function () {
                    // get the version number of the app in the jsonFile
                    var trainingJson = 'data/words/version.json';
                    _this.http.get(trainingJson).subscribe(function (dataJson) {
                        var trainingFile = dataJson.json();
                        var versionJson = trainingFile[0].version;
                        // get the version number of the app in the jsonFile
                        _this.dbService.lastUpdateBd().subscribe(function (dataDb) {
                            // Response from registration attempt
                            if (!dataDb.success) {
                                _this.messageClass = 'alert alert-danger'; // Set an error class
                                _this.message = dataDb.message; // Set an error message
                                _this.processing = false; // Re-enable submit button
                                _this.enableForm(); // Re-enable form
                            }
                            else {
                                _this.messageClass = 'alert alert-success'; // Set a success class
                                _this.message = dataDb.message; // Set a success message
                                var versionMongo = _this.message[0].version;
                                // Load scores of every series
                                _this.dbService.scoresSeriesDownload(_this.username).subscribe(function (scoresSeriesDownload) {
                                    _this.dashboardService.scoresSeries = scoresSeriesDownload.message;
                                    // console.log(this.dashboardService.scoresSeries)
                                });
                                if (versionJson === versionMongo) {
                                    console.log('les deux dates sont similaires');
                                }
                                else {
                                }
                                // After 2 second timeout, navigate to the login page
                                setTimeout(function () {
                                    _this.router.navigate(['/login']); // Redirect to login view
                                }, 2000);
                            }
                        });
                    });
                    // Check if user was redirected or logging in for first time
                    if (_this.previousUrl) {
                        _this.router.navigate([_this.previousUrl]); // Redirect to page they were trying to view before
                    }
                    else {
                        _this.router.navigate(['/']); // Navigate to dashboard view
                    }
                }, 500);
            }
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        // On page load, check if user was redirected to login
        if (this.authGuard.redirectUrl) {
            this.messageClass = 'alert alert-danger'; // Set error message: need to login
            this.message = 'You must be logged in to view that page.'; // Set message
            this.previousUrl = this.authGuard.redirectUrl; // Set the previous URL user was redirected from
            this.authGuard.redirectUrl = undefined; // Erase previous URL
        }
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__services_db_service__["a" /* DbService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__services_db_service__["a" /* DbService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__guards_auth_guard__["a" /* AuthGuard */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_7__guards_auth_guard__["a" /* AuthGuard */]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_select_training_service__["a" /* SelectTrainingService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__services_select_training_service__["a" /* SelectTrainingService */]) === 'function' && _g) || Object, (typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_8__services_dashboard_service__["a" /* DashboardService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_8__services_dashboard_service__["a" /* DashboardService */]) === 'function' && _h) || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/login.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- size Middle -->\n<nav class=\"navbar navbar-expand-lg navbar-light bg-light\" id=\"sizeMiddle\">\n  <a class=\"navbar-brand\" routerLink=\"/\">Magic German</a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n    <ul class=\"navbar-nav\">\n\n      <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/select-training\">Training</a>\n      </li>\n      <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      \n      <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/profile\">My profil</a>\n      </li>\n      \n      <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/\" (click)=\"onLogoutClick()\">Logout</a>\n      </li>\n\n      <!-- <li *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/lecon\">Lessons</a>\n      </li> -->\n\n      <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/login\">Login</a>\n      </li>\n\n      <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions]=\"{exact:true}\">\n        <a class=\"nav-link\" routerLink=\"/register\">Register</a>\n      </li>\n\n\n\n    </ul>\n  </div>\n</nav>"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("../../../../angular2-flash-messages/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(authService, router, flashMessagesService) {
        this.authService = authService;
        this.router = router;
        this.flashMessagesService = flashMessagesService;
    }
    // Function to logout user
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout(); // Logout user
        this.flashMessagesService.show('You are logged out', { cssClass: 'alert-info' }); // Set custom flash message
        this.router.navigate(['/']); // Navigate back to home page
    };
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]) === 'function' && _c) || Object])
    ], NavbarComponent);
    return NavbarComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/navbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Profile Page</h2>\n<ul class=\"list-group\">\n  <li class=\"list-group-item\">Username: {{ username }}</li>\n  <li class=\"list-group-item\">Email: {{ email }}</li>\n</ul>\n"

/***/ }),

/***/ "../../../../../src/app/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = (function () {
    function ProfileComponent(authService) {
        this.authService = authService;
        this.username = '';
        this.email = '';
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Once component loads, get user's data to display on profile
        this.authService.getProfile().subscribe(function (profile) {
            _this.username = profile.user.username; // Set username
            _this.email = profile.user.email; // Set e-mail
        });
    };
    ProfileComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("../../../../../src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/profile/profile.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === 'function' && _a) || Object])
    ], ProfileComponent);
    return ProfileComponent;
    var _a;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-header\">Register Page</h1>\n\n<!-- Custom Success/Error Message -->\n<div class=\"row show-hide-message\">\n  <div [ngClass]=\"messageClass\">\n    {{ message }}\n  </div>\n</div>\n\n<!-- Registration Form -->\n<form [formGroup]=\"form\" (submit)=\"onRegisterSubmit()\">\n\n  <!-- Username Input -->\n  <div class=\"form-group\">\n    <label for=\"username\">Username</label>\n    <div [ngClass]=\"{'has-error': (form.controls.username.errors && form.controls.username.dirty) || (!usernameValid && form.controls.username.dirty), 'has-success': !form.controls.username.errors && usernameValid}\">\n      <input type=\"text\" name=\"username\" class=\"form-control\" autocomplete=\"off\" placeholder=\"*Username\" formControlName=\"username\" (blur)=\"checkUsername()\"  />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.username.errors?.required && form.controls.username.dirty\">This field is required</li>\n        <li *ngIf=\"form.controls.username.errors?.minlength && form.controls.username.dirty || form.controls.username.errors?.maxlength && form.controls.username.dirty \">Minimum characters: 3, Maximum characters: 15</li>\n        <li *ngIf=\"form.controls.username.errors?.validateUsername && form.controls.username.dirty\">Username must not have any special characters</li>\n        <li *ngIf=\"usernameMessage\">{{ usernameMessage }}</li>\n      </ul>\n    </div>\n  </div>\n\n  <!-- Email Input -->\n  <div class=\"form-group\">\n    <label for=\"email\">Email</label>\n    <div [ngClass]=\"{'has-error': (form.controls.email.errors && form.controls.email.dirty) || (!emailValid && form.controls.email.dirty), 'has-success': !form.controls.email.errors && emailValid}\">\n      <input type=\"text\" name=\"email\" class=\"form-control\" autocomplete=\"off\" placeholder=\"*Email\" formControlName=\"email\" (blur)=\"checkEmail()\" />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.email.errors?.required && form.controls.email.dirty\">This field is required</li>\n        <li *ngIf=\"(form.controls.email.errors?.minlength && form.controls.email.dirty || form.controls.email.errors?.maxlength && form.controls.email.dirty ) && form.controls.email.dirty\">Minimum characters: 5, Maximum characters: 30</li>\n        <li *ngIf=\"form.controls.email.errors?.validateEmail && form.controls.email.dirty\">This must be a valid e-mail</li>\n        <li *ngIf=\"emailMessage\">{{ emailMessage}}</li>\n      </ul>\n    </div>\n  </div>\n\n  <!-- Password Input -->\n  <div class=\"form-group\">\n    <label for=\"password\">Password</label>\n    <div [ngClass]=\"{'has-error': (form.controls.password.errors && form.controls.password.dirty), 'has-success': !form.controls.password.errors}\">\n      <input type=\"password\" name=\"password\" class=\"form-control\" autocomplete=\"off\" placeholder=\"*Password\" formControlName=\"password\" />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.password.errors?.required && form.controls.password.dirty\">This field is required</li>\n        <li *ngIf=\"form.controls.password.errors?.minlength && form.controls.password.dirty || form.controls.password.errors?.maxlength && form.controls.password.dirty \">Minimum characters: 8, Maximum characters: 35</li>\n        <li *ngIf=\"form.controls.password.errors?.validatePassword && form.controls.password.dirty\">Must have at least one uppercase, lowercase, special character, and number</li>\n      </ul>\n    </div>\n  </div>\n\n  <!-- Confirm Password Input -->\n  <div class=\"form-group\">\n    <label for=\"confirm\">Confirm Password</label>\n    <div [ngClass]=\"{'has-error': (form.controls.confirm.errors && form.controls.confirm.dirty) || (form.errors?.matchingPasswords && form.controls.confirm.dirty), 'has-success': !form.controls.confirm.errors && !form.errors?.matchingPasswords}\">\n      <input type=\"password\" name=\"confirm\" class=\"form-control\" autocomplete=\"off\" placeholder=\"*Confirm Password\" formControlName=\"confirm\" />\n      <!-- Validation -->\n      <ul class=\"help-block\">\n        <li *ngIf=\"form.controls.confirm.errors?.required && form.controls.confirm.dirty\">This field is required</li>\n        <li *ngIf=\"form.errors?.matchingPasswords && form.controls.confirm.dirty\">Password do not match</li>\n      </ul>\n    </div>\n  </div>\n\n  <!-- Submit Input -->\n  <input [disabled]=\"!form.valid || processing || !emailValid || !usernameValid\" type=\"submit\" class=\"btn btn-primary\" value=\"Submit\" />\n\n</form>\n<!-- Registration Form /-->\n"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = (function () {
    function RegisterComponent(formBuilder, authService, router) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.router = router;
        this.processing = false;
        this.createForm(); // Create Angular 2 Form when component loads
    }
    // Function to create registration form
    RegisterComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // Email Input
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].minLength(5),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].maxLength(30),
                    this.validateEmail // Custom validation
                ])],
            // Username Input
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].minLength(3),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].maxLength(15),
                    this.validateUsername // Custom validation
                ])],
            // Password Input
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].minLength(8),
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].maxLength(35),
                    this.validatePassword // Custom validation
                ])],
            // Confirm Password Input
            confirm: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* Validators */].required] // Field is required
        }, { validator: this.matchingPasswords('password', 'confirm') }); // Add custom validator to form for matching passwords
    };
    // Function to disable the registration form
    RegisterComponent.prototype.disableForm = function () {
        this.form.controls['email'].disable();
        this.form.controls['username'].disable();
        this.form.controls['password'].disable();
        this.form.controls['confirm'].disable();
    };
    // Function to enable the registration form
    RegisterComponent.prototype.enableForm = function () {
        this.form.controls['email'].enable();
        this.form.controls['username'].enable();
        this.form.controls['password'].enable();
        this.form.controls['confirm'].enable();
    };
    // Function to validate e-mail is proper format
    RegisterComponent.prototype.validateEmail = function (controls) {
        // Create a regular expression
        var regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        // Test email against regular expression
        if (regExp.test(controls.value)) {
            return null; // Return as valid email
        }
        else {
            return { 'validateEmail': true }; // Return as invalid email
        }
    };
    // Function to validate username is proper format
    RegisterComponent.prototype.validateUsername = function (controls) {
        // Create a regular expression
        var regExp = new RegExp(/^[a-zA-Z0-9]+$/);
        // Test username against regular expression
        if (regExp.test(controls.value)) {
            return null; // Return as valid username
        }
        else {
            return { 'validateUsername': true }; // Return as invalid username
        }
    };
    // Function to validate password
    RegisterComponent.prototype.validatePassword = function (controls) {
        // Create a regular expression
        var regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
        // Test password against regular expression
        if (regExp.test(controls.value)) {
            return null; // Return as valid password
        }
        else {
            return { 'validatePassword': true }; // Return as invalid password
        }
    };
    // Funciton to ensure passwords match
    RegisterComponent.prototype.matchingPasswords = function (password, confirm) {
        return function (group) {
            // Check if both fields are the same
            if (group.controls[password].value === group.controls[confirm].value) {
                return null; // Return as a match
            }
            else {
                return { 'matchingPasswords': true }; // Return as error: do not match
            }
        };
    };
    // Function to submit form
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        this.processing = true; // Used to notify HTML that form is in processing, so that it can be disabled
        this.disableForm(); // Disable the form
        // Create user object form user's inputs
        var user = {
            email: this.form.get('email').value,
            username: this.form.get('username').value,
            password: this.form.get('password').value,
            wordScores: [],
            declinaisonScores: [],
            genderScores: [],
            irregularVerbsScores: [],
            pluralScores: [],
        };
        // Function from authentication service to register user
        this.authService.registerUser(user).subscribe(function (data) {
            // Resposne from registration attempt
            if (!data.success) {
                _this.messageClass = 'alert alert-danger'; // Set an error class
                _this.message = data.message; // Set an error message
                _this.processing = false; // Re-enable submit button
                _this.enableForm(); // Re-enable form
            }
            else {
                _this.messageClass = 'alert alert-success'; // Set a success class
                _this.message = data.message; // Set a success message
                // After 2 second timeout, navigate to the login page
                setTimeout(function () {
                    _this.router.navigate(['/login']); // Redirect to login view
                }, 2000);
            }
        });
    };
    // Function to check if e-mail is taken
    RegisterComponent.prototype.checkEmail = function () {
        var _this = this;
        // Function from authentication file to check if e-mail is taken
        this.authService.checkEmail(this.form.get('email').value).subscribe(function (data) {
            // Check if success true or false was returned from API
            if (!data.success) {
                _this.emailValid = false; // Return email as invalid
                _this.emailMessage = data.message; // Return error message
            }
            else {
                _this.emailValid = true; // Return email as valid
                _this.emailMessage = data.message; // Return success message
            }
        });
    };
    // Function to check if username is available
    RegisterComponent.prototype.checkUsername = function () {
        var _this = this;
        // Function from authentication file to check if username is taken
        this.authService.checkUsername(this.form.get('username').value).subscribe(function (data) {
            // Check if success true or success false was returned from API
            if (!data.success) {
                _this.usernameValid = false; // Return username as invalid
                _this.usernameMessage = data.message; // Return error message
            }
            else {
                _this.usernameValid = true; // Return username as valid
                _this.usernameMessage = data.message; // Return success message
            }
        });
    };
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === 'function' && _c) || Object])
    ], RegisterComponent);
    return RegisterComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/register.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/select-training/select-training.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#welcomingPhrase{\n    margin-top: 10%;\n    color: #777;\n    font-size: 125%;\n    text-align: center;\n}\n\n#typeOfTraining, #levelOfTraining, #vocabulary, #orderOfTraining {\n    /* height: 85%; */\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    margin: 10% 0% 10% 0%;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n\nbutton {\n    margin: 13px 10px 0px 10px;\n    width:220px;\n    color: #777;\n    text-align: center;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/select-training/select-training.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================\n            Selection of the type of the training\n============================================================  -->\n<div *ngIf=\"this.canChooseType === true\" class=\"step1\">\n\n    <div id=\"welcomingPhrase\">So, what are you into today ?</div>\n    <div id=\"typeOfTraining\">\n        <div *ngFor=\"let typeOfTraining of trainingsType\" (click)=\"selectTypeTraining($event, typeOfTraining)\">\n            <button type=\"button\" class=\"btn btn-default btn-lg\">{{ typeOfTraining }}</button>\n        </div>\n    </div>\n\n</div>\n\n<!-- ============================================================\n             Selection of the level of the training\n============================================================  -->\n<div *ngIf=\"this.canChooseLevel === true\" class=\"step2\">\n    \n    <div id=\"welcomingPhrase\">Alright, which level ?</div>\n    <div id=\"levelOfTraining\">\n        <div *ngFor=\"let levelOfTraining of trainingsLevel\" (click)=\"selectLevelTraining($event, levelOfTraining)\">\n            <button type=\"button\" class=\"btn btn-default btn-lg\">{{ levelOfTraining }}</button>\n        </div>\n    </div>\n\n</div>\n\n\n<!-- ============================================================\n             Selection of the type of Vocabulary\n============================================================  -->\n<div *ngIf=\"this.canChooseVocabulary === true\" class=\"step3\">\n    \n    <div id=\"welcomingPhrase\">Which kind of Vocabulary ?</div>\n    <div id=\"vocabulary\">\n        <div *ngFor=\"let vocabulary of trainingVocabularyType\" (click)=\"selectVocabularyType($event, vocabulary)\">\n            <button type=\"button\" class=\"btn btn-default btn-lg\">{{ vocabulary }}</button>\n        </div>\n    </div>\n\n</div>\n\n\n<!-- ============================================================\n          Selection of the language of the translation\n============================================================  -->\n<div *ngIf=\"this.canChooseOrder === true\" class=\"step4\">\n    \n    <div id=\"welcomingPhrase\">To which language would you like to translate ?</div>\n    <div id=\"orderOfTraining\">\n        <div *ngFor=\"let orderOfTraining of trainingsOrder\" (click)=\"selectOrderTraining($event, orderOfTraining)\">\n            <button type=\"button\" class=\"btn btn-default btn-lg\">{{ orderOfTraining }}</button>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/select-training/select-training.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__select_training__ = __webpack_require__("../../../../../src/app/components/select-training/select-training.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_select_training_service__ = __webpack_require__("../../../../../src/app/services/select-training.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectTrainingComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SelectTrainingComponent = (function () {
    function SelectTrainingComponent(router, authService, selectTraining) {
        this.router = router;
        this.authService = authService;
        this.selectTraining = selectTraining;
        this.trainingsType = ['vocabulary',
            'declinaison',
            'gender',
            'irregularVerbs',
            'plural'
        ];
        this.trainingsLevel = ['All',
            'A21',
            'A22',
            'B11',
            'B12'
        ];
        this.trainingsOrder = ['Deutsch',
            'Français'
        ];
        this.trainingVocabularyType = ['All',
            'Verbe',
            'Nom',
            'Expression',
            'Adverbe',
            'Adjectif',
            'Preposition',
            'Pronom'
        ];
    }
    SelectTrainingComponent.prototype.selectTypeTraining = function (event, typeOfTraining) {
        this.typeTraining = typeOfTraining;
        // Choix supplémentaire pour les types d'entrainements ci-dessous
        if (this.typeTraining === 'vocabulary' ||
            this.typeTraining === 'gender' ||
            this.typeTraining === 'plural') {
            this.canChooseLevel = true;
        }
        else {
            this.canChooseLevel = false;
        }
        // Option supplémentaire pour ce type d'entrainement
        if (this.typeTraining === 'irregularVerbs') {
            this.canChooseOrder = true;
        }
        // Création de l'objet dans le cas des déclinaisons
        if (this.typeTraining === 'declinaison') {
            this.createObject();
        }
        // Sinon
        this.canChooseType = false;
    };
    SelectTrainingComponent.prototype.selectLevelTraining = function (event, levelOfTraining) {
        this.levelTraining = levelOfTraining;
        // Choix supplémentaire pour les types d'entrainements ci-dessous
        if (this.typeTraining === 'vocabulary') {
            this.canChooseVocabulary = true;
        }
        else {
            this.canChooseVocabulary = false;
        }
        // Création de l'objet dans le cas du plural et du gender
        if (this.typeTraining === 'plural' || this.typeTraining === 'gender') {
            this.createObject();
        }
        this.canChooseLevel = false;
    };
    SelectTrainingComponent.prototype.selectVocabularyType = function (event, typeOfVocabulary) {
        this.vocabularyType = typeOfVocabulary;
        // Choix supplémentaire pour les types d'entrainements ci-dessous
        if (this.typeTraining === 'vocabulary') {
            this.canChooseOrder = true;
        }
        else {
            this.canChooseOrder = false;
        }
        this.canChooseVocabulary = false;
    };
    SelectTrainingComponent.prototype.selectOrderTraining = function (event, orderOfTraining) {
        this.orderTraining = orderOfTraining;
        // Création de l'objet pour le vocabulary
        if (this.typeTraining === 'vocabulary') {
            this.createObject();
        }
        // Création de l'objet pour les Verbes Irréguliers
        if (this.typeTraining === 'irregularVerbs') {
            this.createObject();
        }
        this.canChooseOrder = false;
        // direction vers le module d'entrainement
    };
    // Cette fonction est appelée à la fin de chaque sélection afin de créer
    // un objet qui contiendra toutes les caractéristiques de l'entrainement.
    // Envoi de celles-ci par le biais du data service. Esh
    SelectTrainingComponent.prototype.createObject = function () {
        this.selectedTraining = new __WEBPACK_IMPORTED_MODULE_2__select_training__["a" /* SelectTraining */](this.typeTraining, this.levelTraining, this.orderTraining, this.vocabularyType, this.repetitionNumber);
        this.selectTraining.changeMessage(this.selectedTraining);
        this.router.navigate(['/training']);
    };
    SelectTrainingComponent.prototype.ngOnInit = function () {
        this.typeTraining = null;
        this.levelTraining = null;
        this.orderTraining = null;
        this.vocabularyType = null;
        this.repetitionNumber = 15;
        this.canChooseType = true;
        this.canChooseLevel = false;
        this.canChooseOrder = false;
        this.canChooseVocabulary = false;
    };
    SelectTrainingComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-select-training',
            template: __webpack_require__("../../../../../src/app/components/select-training/select-training.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/select-training/select-training.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_select_training_service__["a" /* SelectTrainingService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__services_select_training_service__["a" /* SelectTrainingService */]) === 'function' && _c) || Object])
    ], SelectTrainingComponent);
    return SelectTrainingComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/select-training.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/select-training/select-training.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectTraining; });
var SelectTraining = (function () {
    function SelectTraining(typeTraining, levelTraining, orderTraining, vocabularyType, repetitionNumber) {
        this.typeTraining = typeTraining;
        this.levelTraining = levelTraining;
        this.orderTraining = orderTraining;
        this.vocabularyType = vocabularyType;
        this.repetitionNumber = repetitionNumber;
    }
    SelectTraining.prototype.getTypeTraining = function () {
        return this.typeTraining;
    };
    SelectTraining.prototype.geLevelTraining = function () {
        return this.levelTraining;
    };
    SelectTraining.prototype.getOrderTraining = function () {
        return this.orderTraining;
    };
    SelectTraining.prototype.getVocabularyType = function () {
        return this.vocabularyType;
    };
    SelectTraining.prototype.getRepetitionNumber = function () {
        return this.repetitionNumber;
    };
    return SelectTraining;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/select-training.js.map

/***/ }),

/***/ "../../../../../src/app/components/training/training.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*---------------------------------------------------------------------------*/\n/*------------------------------MIDDLE-SECTION-------------------------------*/\n/*---------------------------------------------------------------------------*/\n.middle-section{\n    text-align: center;\n    padding: 20px 50px 15px 50px;\n}\n\n/*Permet de ne pas avoir un bouton qui dépasse de l'écran. La notion important surpasse les autres informations. */\n#nounTraining, #nounTrainingAnswer, #nounTrainingTip, #verbsTraining, #verbsTrainingAnswer, #verbsTrainingTip{\n    font-size: 1.1em;\n    text-align: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;    \n    color: #777;\n    white-space: normal !important; \n}\n\n/*Permet de ne pas avoir un bouton qui dépasse de l'écran. La notion important surpasse les autres informations. */\n#declinaisonTraining, #declinaisonTrainingAnswer{\n    font-size: 1em;\n    text-align: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    color: #777;\n    white-space: normal !important; \n    right: 10%;\n}\n\n#nounTraining, #verbsTraining, #declinaisonTraining{\n    top: 20%;\n}\n\n#nounTrainingAnswer, #verbsTrainingAnswer, #declinaisonTrainingAnswer{\n    margin: 15px;\n}\n\n/*---------------------------------------------------------------------------*/\n/*------------------------------BOTTOM-SECTION-------------------------------*/\n/*---------------------------------------------------------------------------*/\nfooter{\n    position: absolute;\n    left: 0;\n    bottom: 0%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    width: 100%;\n}\n\n#win-button, #heart-button, #lightbulb-button, #loose-button{\n    padding: 0 8px 0 8px;\n}\n\n#entrainement{\n    margin-bottom: 5%;\n}\n\n.progress {\n    text-align: center;\n    position: absolute;\n    top: 80%;\n    color: #f90;\n    width: 85%;\n    right: 7.5%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/training/training.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<div class=\"middle-section\">\n\n    <!-- ============================================================\n            Display for Vocabulary / Gender / Plural Training\n    ============================================================  -->\n    <div *ngIf=\"this.isNoun === true\" id=\"nounTraining\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\" (click)=\"showResponse()\">\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ results.germanGender }} {{ results.word }} \n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ results.word}}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ results.word}}\n                </div>\n            </div>\n\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ results.frenchGender }} {{ results.translation}} \n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ results.translation}}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ results.translation}}\n                </div>\n            </div>\n        </button>\n    </div>\n\n    <div *ngIf=\"this.isNoun === true && this.responseAsked === true\" id=\"nounTrainingAnswer\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ results.germanGender }} {{ results.word}}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ results.word}}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ results.word}}\n                </div>\n            </div>\n\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ results.frenchGender }} {{ results.translation}} \n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ results.translation}}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ results.translation}}\n                </div>\n            </div>\n        </button>\n    </div>\n\n    <div *ngIf=\"this.isNoun === true && this.tipsAsked === true\" id=\"nounTrainingTip\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ tipWord }}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ tipWord }}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ tipWord }}\n                </div>\n            </div>\n\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                <div *ngIf=\"this.results['wordType'] == 'Nom'\">\n                    {{ tiptranslation }}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Verbe'\">\n                    {{ tiptranslation }}\n                </div>\n                <div *ngIf=\"this.results['wordType'] == 'Expression' ||\n                            this.results['wordType'] == 'Adverbe' ||\n                            this.results['wordType'] == 'Adjectif' ||\n                            this.results['wordType'] == 'Preposition' ||\n                            this.results['wordType'] == 'Pronom'\">\n                    {{ tiptranslation }}\n                </div>\n            </div>\n        </button>\n    </div>\n\n    <!-- ============================================================\n                    Display for irregular verbs Training\n    ============================================================  -->\n    <div *ngIf=\"this.isVerbs === true\" id=\"verbsTraining\" (click)=\"showResponse()\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                {{ results.translation}}\n            </div>\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                {{ results.Infinitif}}\n            </div>\n        </button>\n    </div>\n\n    <div *ngIf=\"this.isVerbs === true && this.responseAsked === true\" id=\"verbsTrainingAnswer\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                {{ results.translation}}\n            </div>\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                {{ results.Infinitif}}\n            </div>\n        </button>\n    </div>\n\n    <div *ngIf=\"this.isVerbs === true && this.tipsAsked === true\" id=\"verbsTrainingTip\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            <div *ngIf=\"this.orderOfTraining == 'Français'\">\n                {{ tipVtranslation }}\n            </div>\n            <div *ngIf=\"this.orderOfTraining == 'Deutsch'\">\n                {{ tipVInfinitif }}\n            </div>\n        </button>\n    </div>\n\n    <!-- ============================================================\n                    Display for declinaison Training\n    ============================================================  -->\n    <div *ngIf=\"this.isDeclinaison === true\" id=\"declinaisonTraining\" (click)=\"showResponse()\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            {{ results.Cas}} / {{ results.gender}} / {{ results.Type}}\n        </button>\n    </div>\n\n    <div *ngIf=\"this.isDeclinaison === true && this.responseAsked === true\" id=\"declinaisonTrainingAnswer\">\n        <button type=\"button\" class=\"btn btn-default btn-lg\">\n            {{ results.Reponse}}\n        </button>\n    </div>\n\n    <!-- ============================================================\n                                Footer\n    ============================================================  -->\n\n    <div>\n\n\n        <div class=\"progress\">\n            <div id=\"bar\" class=\"progress-bar progress-bar-warning\" role=\"progressbar\" [style.width]=\"progressBar()\">\n                {{ progress }} %\n            </div>\n        </div>\n\n        <footer id=\"entrainement\">\n            <button type=\"button\" class=\"btn btn-default\" id=\"loose-button\">\n                <i class=\"far fa-thumbs-down fa-2x\" aria-hidden=\"true\" (click)=\"reduceRepetition('loose')\"></i>\n            </button>\n            <button type=\"button\" class=\"btn btn-default\" id=\"superLike\">\n                <i class=\"far fa-heart fa-2x\" aria-hidden=\"true\" (click)=\"superLike()\"></i>\n            </button>\n\n            <button *ngIf=\"this.isNoun === true\" type=\"button\" class=\"btn btn-default\" id=\"hint\">\n                <i class=\"far fa-lightbulb fa-2x\" aria-hidden=\"true\" (click)=\"displayNounTips()\"></i>\n            </button>\n            <button *ngIf=\"this.isVerbs === true\" type=\"button\" class=\"btn btn-default\" id=\"hint\">\n                <i class=\"far fa-lightbulb fa-2x\" aria-hidden=\"true\" (click)=\"displayVerbsTips()\"></i>\n            </button>\n\n            <button type=\"button\" class=\"btn btn-default\" id=\"win-button\">\n                <i class=\"far fa-thumbs-up fa-2x\" aria-hidden=\"true\" (click)=\"reduceRepetition('win')\"></i>\n            </button>\n        </footer>\n\n    </div>\n\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/training/training.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_select_training_service__ = __webpack_require__("../../../../../src/app/services/select-training.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_db_service__ = __webpack_require__("../../../../../src/app/services/db.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TrainingComponent = (function () {
    function TrainingComponent(selectTraining, dbService, http, router, authService) {
        this.selectTraining = selectTraining;
        this.dbService = dbService;
        this.http = http;
        this.router = router;
        this.authService = authService;
        this.progress = 0;
        this.notation = {};
        // notation: {[key: string]: INote} = {};
        this.wesh = [];
        this.wesh2 = 0;
        this.resultSerieGrade = 0;
        this.resultSerie = {};
        this.date = new Date();
    }
    TrainingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectTraining.currentMessage.subscribe(function (trainingCharacteristics) {
            _this.typeOfTraining = trainingCharacteristics['typeTraining'];
            _this.levelOfTraining = trainingCharacteristics['levelTraining'];
            _this.orderOfTraining = trainingCharacteristics['orderTraining'];
            _this.vocabularyTypeOfTraining = trainingCharacteristics['vocabularyType'];
            _this.repetitionNumber2 = _this.repetitionNumber = trainingCharacteristics['repetitionNumber'];
            _this.tipsNumber = 0;
            _this.responseAsked = false;
            _this.superLikeAsked = false;
            // console.log(this.orderOfTraining)
        });
        this.authService.getProfile().subscribe(function (profile) {
            _this.username = profile.user.username;
        });
        var jsFilePath = this.selectTrainingLoad(this.typeOfTraining); // Gather the required JSON file and launch the proper test
    };
    TrainingComponent.prototype.selectTrainingLoad = function (typeOfTraining) {
        var jsFilePath;
        switch (typeOfTraining) {
            case 'vocabulary':
                jsFilePath = 'data/words/training.json';
                this.nounTraining(this.levelOfTraining, this.vocabularyTypeOfTraining, jsFilePath, this.orderOfTraining);
                break;
            case 'plural':
                jsFilePath = 'data/words/training.json';
                this.orderOfTraining = 'Deutsch';
                this.nounTraining(this.levelOfTraining, 'Nom', jsFilePath, 'Deutsch');
                break;
            case 'gender':
                jsFilePath = 'data/words/training.json';
                this.orderOfTraining = 'Deutsch';
                this.nounTraining(this.levelOfTraining, 'Nom', jsFilePath, 'Deutsch');
                break;
            case 'declinaison':
                jsFilePath = 'data/words/declinaisons.json';
                this.declinaisonTraining(jsFilePath);
                break;
            case 'irregularVerbs':
                jsFilePath = 'data/words/irregularVerbs.json';
                this.verbTraining(this.orderOfTraining, jsFilePath);
                break;
            default:
        }
        return jsFilePath;
    };
    TrainingComponent.prototype.verbTraining = function (order, jsFilePath) {
        var _this = this;
        // Read the result field from the JSON response.
        this.http.get(jsFilePath).subscribe(function (selectTraining) {
            _this.isVerbs = true;
            // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
            _this.vocabulary = selectTraining.json();
            // Tant que le nombre de répétition n'est pas épuisé
            var randomInt = _this.getRandomIntInclusive(0, _this.vocabulary.length); // Creation of a random number
            _this.results = _this.vocabulary[randomInt];
        });
        this.isVerbs = false;
    };
    TrainingComponent.prototype.declinaisonTraining = function (jsFilePath) {
        var _this = this;
        // Read the result field from the JSON response.
        this.http.get(jsFilePath).subscribe(function (selectTraining) {
            _this.isDeclinaison = true;
            // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
            _this.vocabulary = selectTraining.json();
            // Tant que le nombre de répétition n'est pas épuisé
            var randomInt = _this.getRandomIntInclusive(0, _this.vocabulary.length); // Creation of a random number
            _this.results = _this.vocabulary[randomInt];
        });
        this.isDeclinaison = false;
    };
    TrainingComponent.prototype.nounTraining = function (level, wordType, jsFilePath, order) {
        var _this = this;
        // Read the result field from the JSON response.
        this.http.get(jsFilePath).subscribe(function (selectTraining) {
            _this.isNoun = true;
            // Attention gros boloss : toutes les fonctions doivent etre implémentées dans les callback
            _this.vocabulary = selectTraining.json()
                .filter(function (item) { return item['level'] === level || level === 'All'; })
                .filter(function (item) { return item['wordType'] === wordType || wordType === 'All'; });
            // console.log(this.vocabulary)
            // Tant que le nombre de répétition n'est pas épuisé
            var randomInt = _this.getRandomIntInclusive(0, _this.vocabulary.length); // Creation of a random number
            _this.results = _this.vocabulary[randomInt];
            // console.log(this.results)
            if (_this.results['wordType'] === 'Nom') {
                _this.results['germanGender'] = _this.genderDeutschDisplay(_this.results['germanGender']);
                _this.results['frenchGender'] = _this.genderFrenchDisplay(_this.results['frenchGender']);
            }
        });
        this.isNoun = false;
    };
    TrainingComponent.prototype.genderDeutschDisplay = function (determiner) {
        var allDeutschDeterminer = { 'M': 'Der', 'F': 'Die', 'N': 'Das', 'P': 'Die' };
        return allDeutschDeterminer[determiner]; // remplace un 'switch', en plus joli
    };
    TrainingComponent.prototype.genderFrenchDisplay = function (determiner) {
        var allFrenchDeterminer = { 'M': 'Un', 'F': 'Une', 'P': 'Des' };
        return allFrenchDeterminer[determiner]; // remplace un 'switch', en plus joli
    };
    TrainingComponent.prototype.showResponse = function () {
        this.responseAsked = true;
        this.tipsAsked = false;
    };
    TrainingComponent.prototype.displayNounTips = function () {
        if (this.responseAsked === true) {
            return;
        }
        else {
            if (this.tipsNumber < 3) {
                this.tipsAsked = true;
                ++this.tipsNumber;
                this.tipWord = this.results.word.substr(0, this.tipsNumber);
                this.tiptranslation = this.results.translation.substr(0, this.tipsNumber);
            }
            else {
                this.showResponse();
            }
        }
    };
    TrainingComponent.prototype.displayVerbsTips = function () {
        if (this.responseAsked === true) {
            return;
        }
        else {
            if (this.tipsNumber < 3) {
                this.tipsAsked = true;
                ++this.tipsNumber;
                this.tipVInfinitif = this.results['Infinitif'].substr(0, this.tipsNumber);
                this.tipVTraduction = this.results['translation'].substr(0, this.tipsNumber);
            }
            else {
                this.showResponse();
            }
        }
    };
    TrainingComponent.prototype.superLike = function () {
        if (this.superLikeAsked === true) {
            this.superLikeAsked = false;
        }
        else {
            this.superLikeAsked = true;
        }
    };
    Object.defineProperty(TrainingComponent.prototype, "currentScore", {
        get: function () {
            if (!(this.results.id in this.notation)) {
                this.notation[this.results.id] = { note: 0, superlike: this.superLikeAsked };
            }
            return this.notation[this.results.id];
        },
        enumerable: true,
        configurable: true
    });
    TrainingComponent.prototype.reduceRepetition = function (wesh) {
        if (this.repetitionNumber > 0) {
            // Notation
            if (this.responseAsked === true) {
                this.currentScore.note += 0;
            }
            else if (this.responseAsked === false) {
                if (wesh === 'win') {
                    if (this.tipsNumber === 0) {
                        this.currentScore.note += 2;
                    }
                    else if (this.tipsNumber === 1) {
                        this.currentScore.note += 1;
                    }
                    else if (this.tipsNumber >= 1) {
                        this.currentScore.note += 0;
                    }
                }
                else if (wesh === 'loose') {
                    this.currentScore.note += 0;
                }
            }
            var jsFilePath = this.selectTrainingLoad(this.typeOfTraining);
            this.tipsAsked = false;
            this.tipsNumber = 0;
            this.responseAsked = false;
            // console.log(this.notation)
            this.repetitionNumber--;
            this.progressBar();
        }
        else {
            this.calculateGeneralScore(this.notation);
            this.sendScoretoDatabe(this.notation, this.typeOfTraining);
            this.router.navigate(['/']);
        }
    };
    TrainingComponent.prototype.getRandomIntInclusive = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    TrainingComponent.prototype.calculateGeneralScore = function (notation) {
        this.wesh = Object.values(notation).map(function (x) { return x.note; });
        this.wesh2 = this.wesh.reduce(function (acc, res) {
            return acc + res;
        });
        this.resultSerieGrade = this.wesh2 / this.repetitionNumber2;
    };
    TrainingComponent.prototype.sendScoretoDatabe = function (notation, typeOfTraining) {
        this.notation['username'] = this.username;
        this.dbService.sendScoretoDatabe(notation, typeOfTraining).subscribe(function (data) {
            // console.log(data);
        });
        this.resultSerie['resultSerieGrade'] = this.resultSerieGrade;
        this.resultSerie['typeOfTraining'] = this.typeOfTraining;
        this.resultSerie['date'] = this.date;
        this.resultSerie['username'] = this.username;
        this.dbService.sendResultSerietoDatabe(this.resultSerie).subscribe(function (data) {
            // console.log(data);
        });
    };
    TrainingComponent.prototype.progressBar = function () {
        this.progress = Math.round((this.repetitionNumber2 - (this.repetitionNumber)) * 100 / this.repetitionNumber2);
        // console.log(this.progress);
        this.resultProgress = this.progress + '%';
        return this.resultProgress;
    };
    TrainingComponent.prototype.ngOnDestroy = function () {
        this.typeOfTraining = '';
        this.levelOfTraining = '';
        this.orderOfTraining = '';
        this.vocabularyTypeOfTraining = '';
        this.notation = {};
    };
    TrainingComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-training',
            template: __webpack_require__("../../../../../src/app/components/training/training.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/training/training.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_select_training_service__["a" /* SelectTrainingService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_select_training_service__["a" /* SelectTrainingService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_db_service__["a" /* DbService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__services_db_service__["a" /* DbService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */]) === 'function' && _e) || Object])
    ], TrainingComponent);
    return TrainingComponent;
    var _a, _b, _c, _d, _e;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/training.component.js.map

/***/ }),

/***/ "../../../../../src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    // Function to check if user is authorized to view route
    AuthGuard.prototype.canActivate = function (router, state) {
        // Check if user is logge din
        if (this.authService.loggedIn()) {
            return true; // Return true: User is allowed to view route
        }
        else {
            this.redirectUrl = state.url; // Grab previous urul
            this.router.navigate(['/login']); // Return error and route to login page
            return false; // Return false: user not authorized to view page
        }
    };
    AuthGuard = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _b) || Object])
    ], AuthGuard);
    return AuthGuard;
    var _a, _b;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/guards/notAuth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotAuthGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotAuthGuard = (function () {
    function NotAuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    // Function to determine whether user is authorized to view route
    NotAuthGuard.prototype.canActivate = function () {
        // Check if user is logged in
        if (this.authService.loggedIn()) {
            this.router.navigate(['/']); // Return error, route to home
            return false; // Return false: user not allowed to view route
        }
        else {
            return true; // Return true: user is allowed to view route
        }
    };
    NotAuthGuard = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _b) || Object])
    ], NotAuthGuard);
    return NotAuthGuard;
    var _a, _b;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/notAuth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__("../../../../angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        // domain = "http://localhost:8080/"; // Development Domain - Not Needed in Production
        this.domain = window.location.origin + '/'; // Development Domain - Not Needed in Production
    }
    // Function to create headers, add token, to be used in HTTP requests
    AuthService.prototype.createAuthenticationHeaders = function () {
        this.loadToken(); // Get token so it can be attached to headers
        // Headers configuration options
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({
                'Content-Type': 'application/json',
                'authorization': this.authToken // Attach token
            })
        });
    };
    // Function to get token from client local storage
    AuthService.prototype.loadToken = function () {
        this.authToken = localStorage.getItem('token'); // Get token and asssign to variable to be used elsewhere
    };
    // Function to register user accounts
    AuthService.prototype.registerUser = function (user) {
        return this.http.post(this.domain + 'authentication/register', user).map(function (res) { return res.json(); });
    };
    // Function to check if username is taken
    AuthService.prototype.checkUsername = function (username) {
        return this.http.get(this.domain + 'authentication/checkUsername/' + username).map(function (res) { return res.json(); });
    };
    // Function to check if e-mail is taken
    AuthService.prototype.checkEmail = function (email) {
        return this.http.get(this.domain + 'authentication/checkEmail/' + email).map(function (res) { return res.json(); });
    };
    // Function to login user
    AuthService.prototype.login = function (user) {
        return this.http.post(this.domain + 'authentication/login', user).map(function (res) { return res.json(); });
    };
    // Function to logout
    AuthService.prototype.logout = function () {
        this.authToken = null; // Set token to null
        this.user = null; // Set user to null
        localStorage.clear(); // Clear local storage
    };
    // Function to store user's data in client local storage
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('token', token); // Set token in local storage
        localStorage.setItem('user', JSON.stringify(user)); // Set user in local storage as string
        this.authToken = token; // Assign token to be used elsewhere
        this.user = user; // Set user to be used elsewhere
    };
    // Function to get user's profile data
    AuthService.prototype.getProfile = function () {
        this.createAuthenticationHeaders(); // Create headers before sending to API
        return this.http.get(this.domain + 'authentication/profile', this.options).map(function (res) { return res.json(); });
    };
    // Function to check if user is logged in
    AuthService.prototype.loggedIn = function () {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])();
    };
    AuthService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === 'function' && _a) || Object])
    ], AuthService);
    return AuthService;
    var _a;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/dashboard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardService = (function () {
    function DashboardService(http, authService) {
        this.http = http;
        this.authService = authService;
    }
    DashboardService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */]) === 'function' && _b) || Object])
    ], DashboardService);
    return DashboardService;
    var _a, _b;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/dashboard.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/db.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DbService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DbService = (function () {
    function DbService(http, authService) {
        this.http = http;
        this.authService = authService;
        this.domain = this.authService.domain;
    }
    // Function to create headers, add token, to be used in HTTP requests
    DbService.prototype.createAuthenticationHeaders = function () {
        this.authService.loadToken(); // Get token so it can be attached to headers
        // Headers configuration options
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({
                'Content-Type': 'application/json',
                'authorization': this.authService.authToken // Attach token
            })
        });
    };
    DbService.prototype.dbbDownload = function () {
        this.createAuthenticationHeaders();
        return this.http.get(this.domain + 'version/dbbDownload', this.options).map(function (res) { return res.json(); });
    };
    DbService.prototype.scoresWordsDownload = function (username) {
        this.createAuthenticationHeaders();
        return this.http.post(this.domain + 'training/scoresWordsDownload', username, this.options).map(function (res) { return res.json(); });
    };
    // Function to ask the server the new training
    DbService.prototype.lastUpdateBd = function () {
        this.createAuthenticationHeaders();
        return this.http.get(this.domain + 'version/lastUpdateBd', this.options).map(function (res) { return res.json(); });
    };
    DbService.prototype.sendScoretoDatabe = function (results, typeOfTraining) {
        var result;
        this.createAuthenticationHeaders();
        switch (typeOfTraining) {
            case 'vocabulary':
                result = this.http.post(this.domain + 'training/sendResultVocabulary', results, this.options).map(function (res) { return res.json(); });
                break;
            case 'plural':
                result = this.http.post(this.domain + 'training/sendResultPlural', results, this.options).map(function (res) { return res.json(); });
                break;
            case 'gender':
                result = this.http.post(this.domain + 'training/sendResultGender', results, this.options).map(function (res) { return res.json(); });
                break;
            case 'declinaison':
                result = this.http.post(this.domain + 'training/sendResultDeclinaison', results, this.options).map(function (res) { return res.json(); });
                break;
            case 'irregularVerbs':
                result = this.http.post(this.domain + 'training/sendResultIrregularVerbs', results, this.options).map(function (res) { return res.json(); });
                break;
            default:
        }
        return result;
    };
    DbService.prototype.sendResultSerietoDatabe = function (resultSerie) {
        var result;
        this.createAuthenticationHeaders();
        result = this.http.post(this.domain + 'training/sendResultSerie', resultSerie, this.options).map(function (res) { return res.json(); });
        return result;
    };
    DbService.prototype.scoresSeriesDownload = function (username) {
        this.createAuthenticationHeaders();
        return this.http.post(this.domain + 'training/scoresSeriesDownload', username, this.options).map(function (res) { return res.json(); });
    };
    DbService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */]) === 'function' && _b) || Object])
    ], DbService);
    return DbService;
    var _a, _b;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/db.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/select-training.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("../../../../../src/app/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectTrainingService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SelectTrainingService = (function () {
    function SelectTrainingService(http, authService) {
        this.http = http;
        this.authService = authService;
        // On déclare ici un objet vide qui viendra etre populé par le component select training
        // avec les caractéristiques de l'entrainement
        this.messageSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.currentMessage = this.messageSource.asObservable();
        this.domain = this.authService.domain;
    }
    SelectTrainingService.prototype.changeMessage = function (message) {
        this.messageSource.next(message);
    };
    // Function to create headers, add token, to be used in HTTP requests
    SelectTrainingService.prototype.createAuthenticationHeaders = function () {
        this.authService.loadToken(); // Get token so it can be attached to headers
        // Headers configuration options
        this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["RequestOptions"]({
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["Headers"]({
                'Content-Type': 'application/json',
                'Authorization': this.authService.authToken // Attach token
            })
        });
    };
    SelectTrainingService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]) === 'function' && _b) || Object])
    ], SelectTrainingService);
    return SelectTrainingService;
    var _a, _b;
}());
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/select-training.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=D:/Code/Deutsch_App/Deutsch_App_MEAN/client/src/main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map